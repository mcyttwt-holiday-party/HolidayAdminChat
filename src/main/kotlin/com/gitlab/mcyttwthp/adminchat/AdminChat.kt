package com.gitlab.mcyttwthp.adminchat

import com.gitlab.mcyttwthp.adminchat.commands.StaffChatCommand
import com.gitlab.mcyttwthp.adminchat.events.PlayerChatListener
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

class AdminChat : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        this.getCommand("staffchat")?.setExecutor(StaffChatCommand)
        this.server.pluginManager.registerEvents(PlayerChatListener, this)
    }

    companion object {
        lateinit var plugin: JavaPlugin
        val staffChatPlayers = mutableListOf<UUID>()
    }
}