package com.gitlab.mcyttwthp.adminchat.events

import com.gitlab.mcyttwthp.adminchat.AdminChat
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent

object PlayerChatListener : Listener {
    @EventHandler
    fun onPlayerChat(ev: AsyncPlayerChatEvent) {
        if (ev.player.hasPermission("adminchat.ac") && AdminChat.staffChatPlayers.contains(ev.player.uniqueId)) {
            ev.isCancelled = true
            val allStaffChatPlayers = AdminChat.plugin.server.onlinePlayers.filter { it.hasPermission("adminchat.ac") }
            allStaffChatPlayers.forEach {
                it.sendMessage("${ChatColor.GOLD}[SC] ${ev.player.name}: ${ChatColor.GREEN}${ev.message}")
            }
        }
    }
}