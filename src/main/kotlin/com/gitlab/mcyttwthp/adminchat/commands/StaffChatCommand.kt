package com.gitlab.mcyttwthp.adminchat.commands

import com.gitlab.mcyttwthp.adminchat.AdminChat
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

object StaffChatCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("${ChatColor.GOLD}Admin${ChatColor.AQUA}Chat ${ChatColor.GREEN}>> ${ChatColor.RED}You are not a player!")
            return true
        }

        val change = if (AdminChat.staffChatPlayers.contains(sender.uniqueId)) {
            AdminChat.staffChatPlayers.remove(sender.uniqueId)
            false
        } else {
            AdminChat.staffChatPlayers.add(sender.uniqueId)
            true
        }

        sender.sendMessage("${ChatColor.GOLD}Admin${ChatColor.AQUA}Chat ${ChatColor.GREEN}>> ${if (change) "${ChatColor.GREEN}Enabled" else "${ChatColor.RED}Disabled"} ${ChatColor.RESET}your speaking in staff chat.")

        return true
    }
}